package com.qikpod.linefollower;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

public class BluetoothAccessor {

    private static BluetoothDevice hc05 = null;
    private static BluetoothSocket socket = null;

    private final static int REQUEST_ENABLE_BT = 0;

    private BluetoothAccessor(MainActivity parent) {
        // Get the default adapter
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            Logger.Log("Device does not support bluetooth");
        }

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            parent.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        if (pairedDevices.isEmpty()) {
            Logger.Log("Please pair the device first");

        } else {
            for (BluetoothDevice device : pairedDevices) {
                String deviceHardwareAddress = device.getAddress(); // MAC Address

                if (deviceHardwareAddress.equals("20:15:07:02:15:60")) {
                    hc05 = device;
                    break;
                }

            }
        }

        Logger.Log("Found paired " + hc05.getName() + " : " +  hc05.getAddress());

        try {
            socket = hc05.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
            socket.connect();
            Logger.Log(socket.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void init(MainActivity parent) {
        if (socket == null) {
            new BluetoothAccessor(parent);
        } else {
            Logger.LogError("BluetoothAccessor class should be instantiated only once. Restart the Application and try again.");
        }
    }

    public static void write(String txt) throws IOException {
        if (socket == null) {
            return;
        }
        socket.getOutputStream().write(txt.getBytes());
        Logger.Log(txt);
    }
}



