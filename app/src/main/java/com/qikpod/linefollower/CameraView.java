package com.qikpod.linefollower;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Button;

import com.felhr.usbserial.UsbSerialInterface;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CameraView extends SurfaceView implements SurfaceHolder.Callback, Camera.PreviewCallback {
    private SurfaceHolder mHolder;
    private Camera mCamera;

    // Image preview properties
    private int width = 320, height = 240;

    // Application state
    private boolean isRunning = false;
    private boolean isCalibrating = false;
    private boolean isFlashOn = false;

    // List of buttons
    private List<Button> mBlocks = new ArrayList<Button>();

    // median value of each block
    private int[] mBlocksMedian = new int[5];
    private int mBlocksBiggest = 0;
    private int mBlocksLowest = 255;
    // how many pictures were processed in the calibration
    private int calibratingCounter = 0;

    // if the image can be processed
    private boolean canProcess = false;

    // if > colorDivisor, then black
    private int colorDivisor = 126;

    private String lastInput = null;
    private String newInput = null;

    // realign after turning
    boolean realignNeeded = false;

    private MainActivity parent;


    public CameraView(Context context, Camera camera) {
        super(context);

        parent = (MainActivity) context;
        mCamera = camera;

        Camera.Parameters p = mCamera.getParameters();
        p.setPictureSize(width, height);
        p.setPreviewSize(width, height);
        p.setPreviewFrameRate(12);
        mCamera.setParameters(p);

        mCamera.setDisplayOrientation(90);
        //get the holder and set this class as the callback, so we can get camera data here
        mHolder = getHolder();
        mHolder.addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        try {
            // when the surface is created, we can set the camera to draw images in this surfaceholder
            mCamera.setPreviewDisplay(surfaceHolder);
            mCamera.setPreviewCallback(this);
            mCamera.startPreview();

            mBlocks.add((Button) parent.findViewById(R.id.btnExtremeLeft));
            mBlocks.add((Button) parent.findViewById(R.id.btnLeft));
            mBlocks.add((Button) parent.findViewById(R.id.btnStraight));
            mBlocks.add((Button) parent.findViewById(R.id.btnRight));
            mBlocks.add((Button) parent.findViewById(R.id.btnExtremeRight));

        } catch (IOException e) {
            Log.d("ERROR", "Camera error on surfaceCreated " + e.getMessage());
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        //before changing the application orientation, you need to stop the preview, rotate and then start it again
        if (mHolder.getSurface() == null)//check if the surface is ready to receive camera data
            return;

        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            //this will happen when you are trying the camera if it's not running
        }

        //now, recreate the camera preview
        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.setPreviewCallback(this);
            mCamera.startPreview();
        } catch (IOException e) {
            Log.d("ERROR", "Camera error on surfaceChanged " + e.getMessage());
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
    }

    @Override
    public void onPreviewFrame(byte[] bytes, Camera camera) {
        synchronized (this) {
            if (!isCalibrating && !canProcess && isRunning) {
                return; //if we are calibrating the camera, we'll analyze all pictures
                //but if we are running the robot(calibrating = false), we can only analyze it when
                //canProcess = true
                //with canProcess, we'll be able to choose how many pixels will be analyzed per second
            }

            int mBlocksQtd[] = new int[5]; //count number of pixels per block
            Arrays.fill(mBlocksMedian, 0); //we reset the block values

            int initialLine = 295; //where we start to analyze
            int finalLine = 300; //the final line to analyze
            int i, j = 5;

            for (i = 0; i < 240; i++) {
                if (i % 48 == 0) //we dived the 240 line in 5 blocks, each one with 48px
                    j--; //each 48px, we successfully read a block
                int value = 0;
                for (int k = initialLine; k < finalLine; k++) //for each line we read the value of the pixel
                    value += (bytes[k + 320 * i]) & 0xFF; //convert to a byte
                value /= (finalLine - initialLine); //get the median of this pixel

                mBlocksMedian[j] += value; //add the pixel median to the block
                mBlocksQtd[j]++; //add 1 to the number of pixels analyzed in each block.
                // Total will be 48, just use it to be easier to change the code :)
            }

            for (i = 0; i < 5; i++)
                mBlocksMedian[i] = mBlocksMedian[i] / mBlocksQtd[i]; //get the media value of each block

            if (isCalibrating) { //if this method is being called in a calibration, we need to
                doCalibrate(); //recalculate some variables
                return;
            }

            //if we are not processing, we need to display the camera results in the buttons
            for (i = 0; i < 5; i++)
                mBlocks.get(i).setBackgroundColor(mBlocksMedian[i] > colorDivisor ? Color.WHITE : Color.BLACK);


            if (mBlocksMedian[2] < colorDivisor) {

                if (realignNeeded) {
                    newInput = "8 5";
                    realignNeeded = false; // the system is aligned
                } else {
                    newInput = "8";
                }


            } else if (mBlocksMedian[3] < colorDivisor || mBlocksMedian[4] < colorDivisor) {
                newInput = "8 6";
                realignNeeded = true;
            } else if (mBlocksMedian[0] < colorDivisor || mBlocksMedian[1] < colorDivisor) {
                newInput = "8 4";
                realignNeeded = true;
            } else {
                newInput = "5";
            }

            if (lastInput != newInput) {
                lastInput = newInput;
                try {
                    BluetoothAccessor.write(newInput);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            // after each image processed, we'll need to wait until the next frame
            canProcess = false;
        }
    }

    private void doCalibrate() {
        //this method is called each frame
        int i;
        for (i = 0; i < 5; i++) {//so we check the lowest and biggest median of each block
            if (mBlocksMedian[i] > mBlocksBiggest)
                mBlocksBiggest = mBlocksMedian[i];
            if (mBlocksMedian[i] < mBlocksLowest)
                mBlocksLowest = mBlocksMedian[i];
        }

        calibratingCounter++;
        isCalibrating = true;//we do it 100 times, and then we call the stopCalibrate
        if (calibratingCounter == 100)
            stopCalibrate();

    }

    private void stopCalibrate() {
        //this method will calculated the colorDivisor as the median of the lowest and biggest
        colorDivisor = (mBlocksLowest + mBlocksBiggest) / 2;
        isCalibrating = false;

        Logger.Log("New color dividor: " + colorDivisor);
    }

    public void Calibrate() {
        Logger.Log("Calibrating...");
        mBlocksBiggest = 0;
        mBlocksLowest = 255;
        isCalibrating = true;
    }

    public void switchFlash() {
        Camera.Parameters p = mCamera.getParameters();
        if (isFlashOn)
            p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        else
            p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        isFlashOn = !isFlashOn;
        mCamera.setParameters(p);
        Logger.Log("Flash switched");
    }
}