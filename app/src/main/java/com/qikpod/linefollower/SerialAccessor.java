package com.qikpod.linefollower;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;

import com.felhr.usbserial.UsbSerialDevice;
import com.felhr.usbserial.UsbSerialInterface;

import java.io.UnsupportedEncodingException;

public class SerialAccessor {
    private static UsbDevice device;
    private static UsbDeviceConnection connection;
    private static UsbManager usbManager;
    private static UsbSerialDevice serialPort;
    private static String concatenatedData = "";

    private SerialAccessor(MainActivity parent) {

        // Setting up connection to the Arduino
        device = (UsbDevice) parent.getIntent().getParcelableExtra(UsbManager.EXTRA_DEVICE);

        if (device == null) {
            Logger.LogError("Cannot find an Arduino. Are you sure you've connected it?");
            return;
        }

        Logger.Log("Arduino Found");
        Logger.Log("Device name: " + device.getDeviceName());
        Logger.Log("Vendor ID: " + String.valueOf(device.getVendorId()));

        usbManager = (UsbManager) parent.getSystemService(Context.USB_SERVICE);
        connection = usbManager.openDevice(device);

        serialPort = UsbSerialDevice.createUsbSerialDevice(device, connection);

        if (serialPort == null) {
            Logger.LogError("Not able to access the serial port");
            return;
        }

        if (serialPort.open()) {
            serialPort.setBaudRate(9600);
            serialPort.setDataBits(UsbSerialInterface.DATA_BITS_8);
            serialPort.setStopBits(UsbSerialInterface.STOP_BITS_1);
            serialPort.setParity(UsbSerialInterface.PARITY_NONE);
            serialPort.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF);
            serialPort.read(mCallback);
        } else {
            Logger.Log("Serial port not open");
        }
    }

    public static void init(MainActivity parent) {
        if (device == null) {
            new SerialAccessor(parent);
        } else {
            Logger.LogError("SerialAccessor class should be instantiated only once. Restart the Application and try again.");
        }
    }

    private UsbSerialInterface.UsbReadCallback mCallback = new UsbSerialInterface.UsbReadCallback() {

        @Override
        public void onReceivedData(byte[] arg0)
        {
            Logger.Log("DEBUG: Received data from the Arduino");

            try {
                String data = new String(arg0, "UTF-8");

                Logger.Log(data);

                concatenatedData = concatenatedData + data;

                if (concatenatedData.endsWith(":")) {
                    String msg = concatenatedData.substring(0, concatenatedData.length() - 1);
                    Logger.Log(msg);
                    concatenatedData = "";
                }


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    };

    public static void write(String txt) {
        if (serialPort == null) {
            return;
        }
        serialPort.write(txt.getBytes());
        Logger.Log(txt);
    }
}
