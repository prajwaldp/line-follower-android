package com.qikpod.linefollower;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.hardware.Camera;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Camera mCamera = null;
    private CameraView mCameraView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // create the logger instance
        Logger l = Logger.getInstance(this);

        Logger.Log("Starting App");

        // create the serialAccessor instance
        // SerialAccessor.init(this);

        BluetoothAccessor.init(this);


        try{
            Logger.Log("Creating the camera...");
            mCamera = Camera.open();
        } catch (Exception e){
            Logger.LogError("Failed to get camera: " + e.getMessage());
        }

        Logger.Log("Done. Creating the camera preview...");
        if(mCamera != null) {
            // create a SurfaceView to show camera data
            mCameraView = new CameraView(this, mCamera);
            FrameLayout camera_view = (FrameLayout)findViewById(R.id.camera_view);
            // add the SurfaceView to the layout
            camera_view.addView(mCameraView);
        }


        // btn to close the application
        ImageButton imgClose = (ImageButton)findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.exit(0);
            }
        });
        Logger.Log("The application has started successfully!");

        findViewById(R.id.buttonCalibrate).setOnClickListener(this);
        findViewById(R.id.buttonFlash).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonCalibrate:
                mCameraView.Calibrate();
                try {
                    BluetoothAccessor.write("8");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.buttonFlash:
                mCameraView.switchFlash();
                try {
                    BluetoothAccessor.write("8");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
